import subprocess
import requests

class TestAppMethods():
    def setup_class(self):
        self.p = subprocess.Popen(["python","app.py"])

    def teardown_class(self):
        self.p.terminate()

    def test_storeAndGet(self):
        payload = {'key':'BB', 'value':'Gewinner'}
        r = requests.post('http://localhost:5000/submit', data=payload)
        print(r.text)
        assert r.status_code==200
    
    def test_bouncer(self):
        r = requests.get('http://localhost:5000/bouncer')
        assert r.text.find("geschlossen")

    def test_flaky(self):
        assert True