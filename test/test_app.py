import app
class TestAppMethods():

    def test_storeAndGet(self):
        v = "Felix"
        k = "BB"
        app.store(k,v)
        assert app.get(k) == v
    
    def test_bouncer(self):
        v = "Felix"
        k = "BD"
        app.store(k,v)
        assert app.get(k) == v

    def test_flaky(self):
        assert True